# Rompecabezas ERP Martin

Enunciado del rompecabezas:

El demonio ha pensado dos números enteros a y b con 1<a<100, 1<b<100. En secreto le dice a Gauß el producto a * b y a Euler la suma a + b.
Les promete que el que adivine a y b, podrá irse del infierno. Después tienen esta conversación:
· Gauß: No conozco los dos numeros.

· Euler: Lo sabía!

· Gauß: Ahora conozco los dos números!

· Euler: Ah, entonces yo también.

En estos momentos Gauß y Euler se encuentran felizmente en el cielo.


Descripción del código (SPOILER):

La función principal se llama "ks". Esta crea dos matrices de dimensiones K * K. La primera matriz se compone de las multiplicaciones de todos los valores entre 2 y K, y la segunda matriz indica con un 2 si ese valor es una posible solución o con un 1 si no lo es.
Por qué un valor no es una posible solución? Cuando un valor solo se puede dar con una multiplicación implica que Gauß sí que sabría qué números son des del principio, por eso estos valores de descartan.

Posteriormente se buscan los grupos de valores posibles. Estos son todos los que contienen un 2 en una de las anti-diagonales. Estos valores componen la lista *ll_val_tres*. La lista *fin_* contiene los mismos valores pero eliminando los ceros.
De todas las sublistas de *fin_*, se comparan entre ellas para buscar los valores repetidos. Creamos la lista *nar*, la cual tiene exactamente las mismas dimensiones que *fin_* . En esta lista de sublistas ponemos un 1 si el valor no está repetido y un 0 en caso contrario.

Por último, el valor que nos interesa es el que se encuentra en una anti-diagonal donde todos los valores estan repetidos menos él. Es decir, la suma de la sublista de *nar*  es 1.
Por qué? Para que finalmente Euler sepa que valores son, es necesario que solo se encuentre uno en la diagonal que no esté repetido, si no fuera así no podría saber la respuesta.
